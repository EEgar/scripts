#!/bin/bash

echo "
<!DOCTYPE html><html><head>
</head><body>
<h1>Bash Commands in a Web Page</h1>
<h2>REFRESH</h2>
<form action=''>
 <input type='submit' value='Refresh Page'>
</form>
<pre>
"


title1=$(figlet Sensors)
cmd1=$(sensors | sed -e 's/\°/ /g') # browser has problem with degrees, so remove
title2=$(figlet VMStat)
cmd2=$(vmstat)
thebody="$title1\n$cmd1\n$title2\n$cmd2"
 
while true;
  do echo -e "HTTP/1.1 200 OK\n\n$thebody" \
  | nc -l -p 9898 -q 1; 
done

echo "</pre></body></html>"
