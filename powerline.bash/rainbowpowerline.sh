#!/bin/bash

# Crop length of lower directory names to N characters. (default is 2)
shorten=2

# Colors to use for your directory display name backgrounds. Array of Hex Color Values, these are converted to 256 colors for terminal support.
gradient=( "#b82929" "#b0004b" "#97086a" "#cf6e00" "#36e25c" "#00b5b8" "#247ba5" "#48437a" "#4c0031" "#860136" "#b82929" )

tri="🭬" # Use this triangle if using ttf-symbola | The Font https://aur.archlinux.org/font-symbola.git | The Char http://www.fileformat.info/info/unicode/char/1fb6c/index.htm
#tri="" # Use this triangle if using any of the nerdfonts | https://github.com/ryanoasis/nerd-fonts
#tri="" # Use this char if using any of the nerdfonts and you want flames instead of triangles |


function fromhex() {
  hex=$1
  if [[ $hex == "#"* ]]; then
    hex=$(echo $1 | awk '{print substr($0,2)}')
  fi
  r=$(printf '0x%0.2s' "$hex")
  g=$(printf '0x%0.2s' ${hex#??})
  b=$(printf '0x%0.2s' ${hex#????})
  echo -e `printf "%03d" "$(((r<75?0:(r-35)/40)*6*6+(g<75?0:(g-35)/40)*6+(b<75?0:(b-35)/40)+16))"`
}

#Define the string value
text=$(pwd)
# Set / as the delimiter
IFS='/'
#Read the split words into an array based on space delimiter
read -a strarr <<< "$text"
#Count the total words
#echo "There are ${#strarr[*]} words in the text."
indx=0
len=$(( ${#strarr[@]} - 1 ))
printf "\001\e[1m\002"
#otherCols=( "#0087AF" "#585858" )
#gradient=( "#b82929" "#b0004b" "#97086a" "#cf6e00" "#36e25c" "#00b5b8" "#247ba5" "#48437a" "#4c0031" "#860136" "#b82929" )
#rtri="$(node -e 'console.log("🭬")')"
#\360\237\255\254
# Print each value of the array by using the loop
for i in "${!strarr[@]}"
do
  val=${strarr[$i]}
  if [ $indx -eq $len ]
  then
    if [ $indx -eq 0 ]
    then
      val="/"
    fi
    #\001\u00f0\u009f\u00ad\002\u00av
    printf "\001\e[38;5;$(fromhex '#ffffff')m\002\001\e[48;5;$(fromhex ${gradient[$i]})m\002 $val "
    printf "\001\e[0m\002\001\e[38;5;$(fromhex ${gradient[$i]})m\002\001\e[s\002\001$tri\002 \001\e[u\002\001\e[1C\002\001\e[0m\002 \n"
    ((indx++))
  elif [ $indx != 0 ]
  then
    printf "\001\e[38;5;$(fromhex '#ffffff')m\002\001\e[48;5;$(fromhex ${gradient[$i]})m\002 ${val:0:shorten} "
    printf "\001\e[38;5;$(fromhex ${gradient[$i]})m\002\001\e[48;5;$(fromhex ${gradient[(( $i + 1 ))]})m\002\001\e[s\002\001$tri\002 \001\e[u\002\001\e[1C\002"
    ((indx++))
  else
    printf "\001\e[38;5;$(fromhex '#ffffff')m\002\001\e[48;5;$(fromhex ${gradient[$i]})m\002 / "
    printf "\001\e[38;5;$(fromhex ${gradient[$i]})m\002\001\e[48;5;$(fromhex ${gradient[(( $i + 1 ))]})m\002\001\e[s\002\001$tri\002 \001\e[u\002\001\e[1C\002"
    ((indx++))
  fi
done
