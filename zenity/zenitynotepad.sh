#!/bin/bash

# Function to display the text editor
display_editor() {
  # Check if a filename is provided as an argument
  if [ -n "$1" ]; then
    filename="$1"
    text=$(cat "$filename")
  else
    filename=""
    text=""
  fi

  # Display the text editor dialog
  text=$(zenity --text-info --editable --title="Text Editor" --filename="$filename" --text="$text" --width=500 --height=400 --extra-button="Open File" --cancel-label="Cancel" --ok-label="Save" --extra-button="Save As" )

  button_clicked=$?
    echo "text" + $text
    echo $filename ":filename if present"
    echo "button clicked" + $button_clicked

  # Check if the user clicked "Save As"
  if [[ "$text" == "Save As" ]]; then
    # Prompt for a file to save
    
    alltext=$(cat "$filename")
    
    new_filenameas=$(zenity --file-selection --save --confirm-overwrite --title="Save File As")
 
    if [ $? -eq 0 ]; then
  
      echo "Write Saving As....."
      #echo "$filename" >> "$new_filenameas"
      #echo "$1" >> "$new_filenameas"
      #echo "$text" >> "$new_filenameas"
      #echo "entry while save as" + "$entry"
      echo "$alltext" > "$new_filenameas"
      
      
      zenity --info --title="Text Editor" --text="File saved As: $new_filenameas"
    fi
  fi


    if [[ "$text" = "Cancel" ]]; then
      # User clicked "Exit or Cancel" or closed the dialog
      echo "Exit"+ "$text"
    fi



  # Check if the user clicked "OK? SAVE?"
  if [ $button_clicked -eq 0 ]; then
    # Check if a filename is provided
    if [[ -n "$filename" ]]; then
      # Save the text to the existing file
      echo "$text" > "$filename"
      # Notify only if a file was saved
      if [[ $? -eq 0 ]]; then
        zenity --info --title="Text Editor" --text="File saved: $filename"
      fi
    else
      # Prompt for a new filename and save the text
      new_filename=$(zenity --file-selection --save --confirm-overwrite)
      if [ $? -eq 0 ] && [ -n "$new_filename" ]; then
        echo "$text" > "$new_filename"
        # Notify only if a file was saved
        if [[ $? -eq 0 ]]; then
          zenity --info --title="Text Editor" --text="File saved: $new_filename"
        fi
      fi
    fi
  elif [ $button_clicked -eq 1 ]; then
    # Check if the user clicked "Open File"
    # Prompt for a file to open
    filename=$(zenity --file-selection --title="Open File")
    if [ $? -eq 0 ] && [ -n "$filename" ]; then
      # Read the content of the file
      text=$(cat "$filename")
      # Call the display_editor function recursively with the selected file
      display_editor "$filename"
    fi
  fi
}

# Display the text editor
display_editor "$1"

