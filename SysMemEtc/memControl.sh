ps -eo size,pid,user,command --sort -size  | \
    awk '{ hr=$1/1024 ; printf("%13.2f Mb ",hr) } { for ( x=4 ; x<=NF ; x++ ) { printf("%s ",$x) } print "" }' |\
    cut -d "" -f2 | cut -d "-" -f1

#cat /proc/1234/smaps
#/proc/19420$ cat status
#
#ps aux
#will give you Virtual Size (VSZ)

#smem
#pmap
#ps_mem -p <pid>
#ps aux --sort pmem
#ps auxf
# ps -eo size,pid,user,command --sort -size | awk '{ hr=$1/1024 ; printf("%13.2f Mb ",hr) } { for ( x=4 ; x<=NF ; x++ ) { printf("%s ",$x) } print "" }' | awk '{total=total + $1} END {print total}'

###* actual memory usage  of pid or application

#ps -eo size,pid,user,command -f   | \
#    awk '{ hr=$1/1024 ; printf("%13.2f Mb ",hr) } { for ( x=4 ; x<=NF ; x++ ) { printf("%s ",$x) } print "" }' |\
#    cut -d "" -f2 | cut -d "-" -f1

