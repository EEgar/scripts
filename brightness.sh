#!/bin/sh
# by stemsee
BRAND=$(ls /sys/class/backlight/ | grep _backlight)
yad --title="Brightness $BRAND" --height=400 --width=40 \
--no-buttons --on-top --vertical --scale \
--print-partial --min-value=40 --max-value=$(cat /sys/class/backlight/"$BRAND"/max_brightness) --step=1 --value=$(cat /sys/class/backlight/"$BRAND"/brightness) | while read line; do echo "$line" > /sys/class/backlight/"$BRAND"/brightness; sleep 0.01; done &
