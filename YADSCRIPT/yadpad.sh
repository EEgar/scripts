#!/bin/bash

# Function to display the text editor
display_editor() {
  # Check if a filename is provided as an argument
  if [ -n "$1" ]; then
    filename="$1"
    text=$(cat "$filename")
  else
    filename=""
    text=""
  fi

  # Display the text editor dialog
  text=$(yad --text-info --editable --title="Text Editor" --filename="$filename" --text="$text" --width=500 --height=400 --button="Open File:2" --button="Cancel:1" --button="Save As:3" --button="Save:0")

  button_clicked=$?
  echo "text: $text"
  echo "filename: $filename"
  echo "button clicked: $button_clicked"

  case $button_clicked in
    0) # OK/Save
      if [[ -n "$filename" ]]; then
        # Save the text to the existing file
        echo "$text" > "$filename"
        # Notify only if a file was saved
        if [[ $? -eq 0 ]]; then
          yad --info --title="Text Editor" --text="File saved: $filename"
        fi
      else
        # Prompt for a new filename and save the text
        new_filename=$(yad --file --save --title="Save File")
        if [ $? -eq 0 ] && [ -n "$new_filename" ]; then
          echo "$text" > "$new_filename"
          # Notify only if a file was saved
          if [[ $? -eq 0 ]]; then
            yad --info --title="Text Editor" --text="File saved: $new_filename"
          fi
        fi
      fi
      ;;

    3) # Save As
      alltext=$(cat "$filename")
      new_filenameas=$(yad --file --save --title="Save File As")
      if [ $? -eq 0 ] && [ -n "$new_filenameas" ]; then
        echo "$alltext" > "$new_filenameas"
        yad --info --title="Text Editor" --text="File saved As: $new_filenameas"
      fi
      ;;

    2) # Open File
      filename=$(yad --file --title="Open File")
      if [ $? -eq 0 ] && [ -n "$filename" ]; then
        text=$(cat "$filename")
        display_editor "$filename"
      fi
      ;;

    1) # Cancel or Close
      echo "Exit"
      exit
      ;;
  esac
}

# Display the text editor
display_editor "$1"

