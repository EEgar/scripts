!/bin/bash

dialog=$(yad --title "Select Directory" --form --field=Choose:DIR)
echo $dialog | awk 'BEGIN {FS="|" } { print $1 }'
