#!/bin/bash
exec xfce4-terminal --maximize -H -x sh -c 'curl -s "https://archlinux.org/mirrorlist/?country=FR&country=DE&protocol=https&use_mirror_status=on" | sed -e 's/^#Server/Server/' -e '/^#/d' | rankmirrors -n 5 - > ~/mirrorlist-arch; sudo cp ~/mirrorlist-arch /etc/pacman.d/mirrorlist-arch; cat /etc/pacman.d/mirrorlist-arch; exec bash;'
 
## If needed, run first : sudo pacman -Sy pacman-contrib && sudo touch /etc/pacman.d/mirrorlist-arch
