picom --experimental-backends -b

picom -r 30 -i 0.83 -c --corner-radius 9 --blur-method dual_kawase   --blur-strength 10 --backend glx --force-win-blend --config /dev/null   --force-win-blend -o 0.4 -l -30 -r 60 --opacity-rule 40:'name="TaskBar"' --transparent-clipping -b
